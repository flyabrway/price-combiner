package com.flyabrway.price.util;

import com.flyabrway.price.domain.model.PriceModel;

import java.util.*;

public class DefaultPriceCombiner implements PriceCombiner {

    @Override
    public Collection<PriceModel> combine(Collection<PriceModel> oldPrices, Collection<PriceModel> newPrices)
    {
        final NavigableMap<DateRange, PriceModel> oldPricesMap = fill(oldPrices);
        final NavigableMap<DateRange, PriceModel> newPricesMap = fill(newPrices);

        final Queue<PriceModel> result = new PriorityQueue<>(Comparator.comparing(PriceModel::getBeginDate));

        while (notEmptyMaps(oldPricesMap, newPricesMap))
        {

            final Map.Entry<DateRange, PriceModel> oldEntry = oldPricesMap.pollLastEntry();
            final Map.Entry<DateRange, PriceModel> newEntry = newPricesMap.pollLastEntry();

            if (newEntry.getKey().includeRange(oldEntry.getKey()))
            {
                Map.Entry<DateRange, PriceModel> lastIncludedEntry = oldEntry;

                while (oldPricesMap.lastEntry() != null
                        && newEntry.getKey().includeRange(oldPricesMap.lastEntry().getKey()))
                {
                    lastIncludedEntry = oldPricesMap.pollLastEntry();
                }

                final Optional<DateRange> balanceRangeOpt = newEntry.getKey().searchOuterRange(lastIncludedEntry.getKey(), false);

                if (balanceRangeOpt.isPresent() && balanceRangeOpt.get().isEmpty())
                {
                    result.add(newEntry.getValue());
                }
                else if (balanceRangeOpt.isPresent() && balanceRangeOpt.get().isNotEmpty())
                {
                    newPricesMap.put(balanceRangeOpt.get(), copy(newEntry.getValue(), balanceRangeOpt.get()));
                    result.add(copy(newEntry.getValue(), newEntry.getKey().compress(lastIncludedEntry.getKey(), false)));
                }

            }
            else if (oldEntry.getKey().includeRange(newEntry.getKey()))
            {
                final List<Map.Entry<DateRange, PriceModel>> includedNewEntries = new ArrayList<>();
                includedNewEntries.add(newEntry);

                while (newPricesMap.lastEntry() != null
                        && oldEntry.getKey().includeRange(newPricesMap.lastEntry().getKey()))
                {
                    includedNewEntries.add(newPricesMap.pollLastEntry());
                }

                result.addAll(mergeIncludedPrices(includedNewEntries, oldEntry));

                final Optional<DateRange> balanceRangeOpt = oldEntry.getKey().searchOuterRange(
                        includedNewEntries.get(includedNewEntries.size() - 1).getKey(), false
                );
                balanceRangeOpt.filter(DateRange::isNotEmpty).ifPresent(balanceRange -> oldPricesMap.put(balanceRange, copy(oldEntry.getValue(), balanceRange)));
            }
            else if (oldEntry.getKey().overlap(newEntry.getKey()))
            {
                final Optional<DateRange> overlappedOpt = oldEntry.getKey().searchNonOverlap(newEntry.getKey(), true);

                if (overlappedOpt.isPresent() && overlappedOpt.get().isNotEmpty())
                {
                    final DateRange overlappedRange = overlappedOpt.get();
                    if (oldEntry.getKey().includeRange(overlappedRange))
                    {
                        result.add(copy(oldEntry.getValue(), overlappedRange));
                        newPricesMap.put(newEntry.getKey(), newEntry.getValue());
                    }
                    else if (newEntry.getKey().includeRange(overlappedRange))
                    {
                        result.add(newEntry.getValue());

                        final DateRange newOldEntryRange = new DateRange(oldEntry.getKey().begin(), newEntry.getKey().begin());
                        final PriceModel newOldEntryPrice = copy(oldEntry.getValue(), newOldEntryRange);
                        oldPricesMap.put(newOldEntryRange, newOldEntryPrice);
                    }
                }
            }
            else
            {
                if (oldEntry.getKey().laterThan(newEntry.getKey()))
                {
                    result.add(oldEntry.getValue());
                    newPricesMap.put(newEntry.getKey(), newEntry.getValue());
                }
                else if (newEntry.getKey().laterThan(oldEntry.getKey()))
                {
                    result.add(newEntry.getValue());
                    oldPricesMap.put(oldEntry.getKey(), oldEntry.getValue());
                }
            }
        }

        while(!oldPricesMap.isEmpty())
        {
            result.add(oldPricesMap.pollLastEntry().getValue());
        }

        while(!newPricesMap.isEmpty())
        {
            result.add(newPricesMap.pollLastEntry().getValue());
        }

        return process(result);
    }

    private NavigableMap<DateRange, PriceModel> fill(final Collection<PriceModel> prices)
    {
        final NavigableMap<DateRange, PriceModel> pricesMap = new TreeMap<>(Comparator.comparing(DateRange::begin));
        return fill(pricesMap, prices);
    }

    private NavigableMap<DateRange, PriceModel> fill(final NavigableMap<DateRange, PriceModel> pricesMap,
                                                     final Collection<PriceModel> prices)
    {
        prices.forEach(price ->
                pricesMap.putIfAbsent(new DateRange(price.getBeginDate(), price.getEndDate()), price)
        );
        return pricesMap;
    }

    private boolean notEmptyMaps(final Map<DateRange, PriceModel>... maps)
    {
        return Arrays.stream(maps).noneMatch(Map::isEmpty);
    }

    /**
     * It is assumed, that list of new prices already contains all included prices.
     */
    private List<PriceModel> mergeIncludedPrices(final List<Map.Entry<DateRange, PriceModel>> news,
                                                final Map.Entry<DateRange, PriceModel> scope)
    {
        final List<PriceModel> result = new ArrayList<>();

        DateRange tempRange = null;
        DateRange keyScopeRange = scope.getKey();
        PriceModel valueScopePrice = scope.getValue();

        for (final Map.Entry<DateRange, PriceModel> entry : news)
        {
            if (tempRange == null)
            {
                tempRange = new DateRange(scope.getKey().end(), scope.getKey().end());
            }

            final Optional<DateRange> outerRange = keyScopeRange.searchOuterRange(entry.getKey(), true);

            if (outerRange.isPresent() && outerRange.get().isNotEmpty())
            {
                tempRange = tempRange.expand(outerRange.get(), false);
                result.add(copy(valueScopePrice, outerRange.get()));
            }
            else
            {
                tempRange = tempRange.expand(entry.getKey(), false);
            }

            tempRange = tempRange.expand(entry.getKey(), false);
            result.add(entry.getValue());

            keyScopeRange = new DateRange(keyScopeRange.begin(), tempRange.begin());
        }

        return result;
    }

    private PriceModel copy(final PriceModel priceModel, final DateRange dateRange)
    {
        final PriceModel copy = new PriceModel();

        copy.setID(priceModel.getID());
        copy.setProductCode(priceModel.getProductCode());
        copy.setNumber(priceModel.getNumber());
        copy.setDepartment(priceModel.getDepartment());
        copy.setValue(priceModel.getValue());

        copy.setBeginDate(dateRange.begin());
        copy.setEndDate(dateRange.end());

        return copy;
    }

    private Collection<PriceModel> process(final Queue<PriceModel> queue)
    {
        final List<PriceModel> processedResult = new ArrayList<>();

        while(!queue.isEmpty())
        {
            PriceModel current = queue.poll();
            PriceModel next;

            while ((next = queue.peek()) != null
                    && current.getValue() != null
                    && next.getValue() != null
                    && next.getValue().equals(current.getValue()))
            {
                final DateRange currentRange = new DateRange(current.getBeginDate(), current.getEndDate());
                final DateRange nextRange = new DateRange(next.getBeginDate(), next.getEndDate());

                final DateRange expandedRange = currentRange.expand(nextRange);

                current.setBeginDate(expandedRange.begin());
                current.setEndDate(expandedRange.end());

                queue.poll();
            }

            processedResult.add(current);
        }

        return processedResult;
    }

}
