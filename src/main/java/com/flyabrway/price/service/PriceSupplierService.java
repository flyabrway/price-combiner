package com.flyabrway.price.service;

import com.flyabrway.price.domain.dto.PriceDTO;

import java.util.Collection;

public interface PriceSupplierService {

    Collection<PriceDTO> getProductPrices(Long productID);

}
