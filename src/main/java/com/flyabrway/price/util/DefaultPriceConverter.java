package com.flyabrway.price.util;

import com.flyabrway.price.domain.dto.PriceDTO;
import com.flyabrway.price.domain.model.PriceModel;

public class DefaultPriceConverter implements PriceConverter {

    @Override
    public PriceModel toPriceModel(final PriceDTO source) {
        final PriceModel target = new PriceModel();

        target.setID(source.getID());
        target.setProductCode(source.getProductCode());
        target.setNumber(source.getNumber());
        target.setDepartment(source.getDepartment());
        target.setBeginDate(source.getBeginDate());
        target.setEndDate(source.getEndDate());
        target.setValue(source.getValue());

        return target;
    }

}
