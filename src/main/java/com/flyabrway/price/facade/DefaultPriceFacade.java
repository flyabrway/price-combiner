package com.flyabrway.price.facade;

import com.flyabrway.price.domain.dto.PriceDTO;
import com.flyabrway.price.domain.model.PriceModel;
import com.flyabrway.price.util.PriceCombiner;
import com.flyabrway.price.util.PriceConverter;
import com.flyabrway.price.service.PriceService;
import com.flyabrway.price.service.PriceSupplierService;

import java.util.Collection;
import java.util.stream.Collectors;

public class DefaultPriceFacade implements PriceFacade {

    private final PriceService priceService;

    private final PriceSupplierService priceSupplierService;

    private final PriceConverter priceConverter;

    private final PriceCombiner priceCombiner;

    public DefaultPriceFacade(final PriceService priceService,
                              final PriceSupplierService priceSupplierService,
                              final PriceConverter priceConverter,
                              final PriceCombiner priceCombiner) {
        this.priceService = priceService;
        this.priceSupplierService = priceSupplierService;
        this.priceConverter = priceConverter;
        this.priceCombiner = priceCombiner;
    }

    @Override
    public Collection<PriceModel> combineProductPrices(final Long productID) {
        final Collection<PriceDTO> newProductPrices = priceSupplierService.getProductPrices(productID);
        final Collection<PriceModel> oldProductPrices = priceService.getProductPrices(productID);

        final Collection<PriceModel> newProductPriceModels = newProductPrices.stream()
                .map(priceConverter::toPriceModel)
                .collect(Collectors.toList());

        return priceCombiner.combine(oldProductPrices, newProductPriceModels);
    }

}
