package com.flyabrway.price.util;

import com.flyabrway.price.domain.model.PriceModel;

import java.util.Collection;

public interface PriceCombiner {

    Collection<PriceModel> combine(Collection<PriceModel> oldPrices, Collection<PriceModel> newPrices);

}
