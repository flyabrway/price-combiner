package com.flyabrway.price.facade;

import com.flyabrway.price.domain.model.PriceModel;

import java.util.Collection;

public interface PriceFacade {

    Collection<PriceModel> combineProductPrices(Long productID);

}
