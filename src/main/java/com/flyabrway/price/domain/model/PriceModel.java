package com.flyabrway.price.domain.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "ID", "productCode", "number", "department", "value" } )
@EqualsAndHashCode(exclude = { "ID", "productCode", "number", "department", "value" } )
public class PriceModel {

    private Long ID;

    private String productCode;

    private Integer number;

    private Integer department;

    private LocalDate beginDate;

    private LocalDate endDate;

    private Long value;

}
