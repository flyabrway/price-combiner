package com.flyabrway.price.util;

import java.time.LocalDate;
import java.util.*;

public class DateRange {

    private LocalDate begin;
    private LocalDate end;

    public DateRange(final LocalDate begin, final LocalDate end) {
        validate(begin, end);

        this.begin = begin;
        this.end = end;
    }

    private void validate(final LocalDate begin, final LocalDate end) {
        if (begin == null || end == null)
            throw new IllegalArgumentException("Illegal arguments. They can not not be null.");

        if (begin.isAfter(end))
            throw new IllegalArgumentException("Illegal arguments: " + begin + " goes after " + end + ".");
    }

    public LocalDate begin() {
        return begin;
    }

    public LocalDate end() {
        return end;
    }

    /**
     * Returns true, if date includes within this range.
     */
    public boolean includeDate(final LocalDate date) {
        return !date.isBefore(begin) && !date.isAfter(end);
    }

    /**
     * Returns true, if specified params are included into this range.
     */
    public boolean includeRange(final LocalDate begin, final LocalDate end) {
        return includeRange(new DateRange(begin, end));
    }

    /**
     * Returns true, if specified range is included into this range.
     */
    public boolean includeRange(final DateRange range) {
        return this.includeDate(range.begin) && this.includeDate(range.end);
    }

    /**
     * Searches for non included range.
     * Requires that specified range must be included into this range, otherwise returns {@link Optional#empty()}.
     * @param upper determines whether to search for a higher from included location or not.
     */
    public Optional<DateRange> searchOuterRange(final DateRange range, final boolean upper) {
        if (!this.includeRange(range)) {
            return Optional.empty();
        }

        final DateRange result = upper ? new DateRange(range.end, this.end) : new DateRange(this.begin, range.begin);
        return Optional.of(result);
    }

    /**
     * Returns true, if this range overlaps specified range.
     */
    public boolean overlap(final DateRange range) {
        return this.includeRange(range) || range.includeDate(begin) || range.includeDate(end);
    }

    /**
     * Searches for non overlap range.
     * Requires that there is an overlap and a date inclusion. Otherwise returns {@link Optional#empty()}}
     * @param upper determines whether to search for a higher from overlapped location or not.
     */
    public Optional<DateRange> searchNonOverlap(final DateRange range, final boolean upper) {
        if (!this.overlap(range)) {
            return Optional.empty();
        }

        final DateRange lower, higher, result;

        if (this.begin.compareTo(range.begin) < 0) {
            lower = this;
            higher = range;
        } else {
            lower = range;
            higher = this;
        }

        if (upper) {
            result = new DateRange(lower.end, higher.end);
        } else {
            result = new DateRange(lower.begin, higher.begin);
        }

        return Optional.of(result);
    }

    /**
     * Determines a range gap between this range and specified.
     */
    public Optional<DateRange> gap(final DateRange range) {
        if (this.overlap(range)) {
            return Optional.empty();
        }

        final DateRange lower, higher;

        if (this.begin.compareTo(range.begin) < 0) {
            lower = this;
            higher = range;
        } else {
            lower = range;
            higher = this;
        }

        return Optional.of(new DateRange(lower.end, higher.begin));
    }

    /**
     * Splits this range into parts by specified range.
     * If passed range is not included into this range, {@link Collections#emptyList()} will be returned.
     * @return set of ranges splitted by specified range.
     */
    public List<DateRange> split(final DateRange range) {
        if (!this.includeRange(range)) {
            return Collections.emptyList();
        }

        return Arrays.asList(
                new DateRange(this.begin, range.begin),
                range,
                new DateRange(range.end, this.end)
        );
    }

    /**
     * Expands the range between this range and specified.
     */
    public DateRange expand(final DateRange range) {
        final LocalDate begin = this.begin.isBefore(range.begin) ? this.begin : range.begin;
        final LocalDate end = this.end.isBefore(range.end) ? range.end : this.end;

        return new DateRange(begin, end);
    }

    /**
     * Expands the range between this range and specified.
     * @param upper specifies the side for expanding
     */
    public DateRange expand(final DateRange range, boolean upper) {
        if (upper) {
            return this.end.isBefore(range.end) ?
                    new DateRange(this.begin, range.end) : new DateRange(this.begin, this.end);
        } else {
            return this.begin.isAfter(range.begin) ?
                    new DateRange(range.begin, this.end) : new DateRange(this.begin, this.end);
        }
    }

    public DateRange compress(final DateRange range) {
        final LocalDate begin = this.begin.isAfter(range.begin) ? this.begin : range.begin;
        final LocalDate end = this.end.isBefore(range.end) ? this.end : range.end;

        return new DateRange(begin, end);
    }

    /**
     * Compress the range between this range and specified.
     * @param upper specifies the side for compressing
     */
    public DateRange compress(final DateRange range, boolean upper) {
        if (upper) {
            return this.end.isAfter(range.end) ? new DateRange(this.begin, range.end) : new DateRange(this.begin, this.end);
        } else {
            return this.begin.isBefore(range.begin) ?
                    new DateRange(range.begin, this.end) : new DateRange(this.begin, this.end);
        }
    }

    /**
     * Returns true if this range contains same {@link DateRange#begin()} and {@link DateRange#end()} values.
     */
    public boolean isEmpty() {
        return begin.equals(end);
    }

    /**
     * Returns true if this range does not contain same {@link DateRange#begin()} and {@link DateRange#end()} values.
     */
    public boolean isNotEmpty() {
        return !begin.equals(end);
    }

    /**
     * Return true if the start date is less than specified.
     */
    public boolean earlierThan(final DateRange range) {
        return this.begin.isBefore(range.begin);
    }

    /**
     * Return true if the end date is greater than specified.
     */
    public boolean laterThan(final DateRange range) {
        return this.end.isAfter(range.end);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final DateRange range = (DateRange) o;
        return Objects.equals(begin, range.begin) && Objects.equals(end, range.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(begin, end);
    }

}
