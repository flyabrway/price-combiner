package com.flyabrway.price.util;

import com.flyabrway.price.domain.dto.PriceDTO;
import com.flyabrway.price.domain.model.PriceModel;

public interface PriceConverter {

    PriceModel toPriceModel(PriceDTO source);

}
