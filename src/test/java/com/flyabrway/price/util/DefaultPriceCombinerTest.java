package com.flyabrway.price.util;

import com.flyabrway.price.domain.model.PriceModel;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

public class DefaultPriceCombinerTest {

    private static final Integer YEAR = 2000;
    private static final Month JANUARY = Month.JANUARY;
    private static final Month FEBRUARY = Month.FEBRUARY;

    private static final PriceCombiner priceCombiner = new DefaultPriceCombiner();

    private Collection<PriceModel> oldPrices = new ArrayList<>();
    private Collection<PriceModel> newPrices = new ArrayList<>();

    @Test
    public void testCombinePrices_1() {

        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 30)))
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 10))
                        .endDate((LocalDate.of(YEAR, JANUARY, 20)))
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 10))
                        .endDate((LocalDate.of(YEAR, JANUARY, 20)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 20))
                        .endDate((LocalDate.of(YEAR, JANUARY, 30)))
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_2() {

        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 20)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 20))
                        .endDate((LocalDate.of(YEAR, JANUARY, 30)))
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 10))
                        .endDate((LocalDate.of(YEAR, JANUARY, 25)))
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 10))
                        .endDate((LocalDate.of(YEAR, JANUARY, 25)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 25))
                        .endDate((LocalDate.of(YEAR, JANUARY, 30)))
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_3() {

        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 10))
                        .endDate((LocalDate.of(YEAR, JANUARY, 20)))
                        .value(87L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 20))
                        .endDate((LocalDate.of(YEAR, JANUARY, 30)))
                        .value(90L)
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 15)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 15))
                        .endDate((LocalDate.of(YEAR, JANUARY, 25)))
                        .value(85L)
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 15)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 15))
                        .endDate((LocalDate.of(YEAR, JANUARY, 25)))
                        .value(85L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 25))
                        .endDate((LocalDate.of(YEAR, JANUARY, 30)))
                        .value(90L)
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_4() {

        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 2))
                        .endDate((LocalDate.of(YEAR, JANUARY, 5)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 6))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 3)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 4))
                        .endDate((LocalDate.of(YEAR, JANUARY, 7)))
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 3)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 3))
                        .endDate((LocalDate.of(YEAR, JANUARY, 4)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 4))
                        .endDate((LocalDate.of(YEAR, JANUARY, 7)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 7))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_5() {
        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 3)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 4))
                        .endDate((LocalDate.of(YEAR, JANUARY, 7)))
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 2))
                        .endDate((LocalDate.of(YEAR, JANUARY, 5)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 6))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 2)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 2))
                        .endDate((LocalDate.of(YEAR, JANUARY, 5)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 6)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 6))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_6() {

        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 11))
                        .endDate((LocalDate.of(YEAR, JANUARY, 16)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 17))
                        .endDate((LocalDate.of(YEAR, JANUARY, 22)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 23))
                        .endDate((LocalDate.of(YEAR, JANUARY, 24)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 25))
                        .endDate((LocalDate.of(YEAR, JANUARY, 26)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 27))
                        .endDate((LocalDate.of(YEAR, JANUARY, 28)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 30))
                        .endDate((LocalDate.of(YEAR, JANUARY, 31)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, FEBRUARY, 1))
                        .endDate((LocalDate.of(YEAR, FEBRUARY, 2)))
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 6)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 7))
                        .endDate((LocalDate.of(YEAR, JANUARY, 8)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 9))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 11))
                        .endDate((LocalDate.of(YEAR, JANUARY, 16)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 18))
                        .endDate((LocalDate.of(YEAR, JANUARY, 19)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 20))
                        .endDate((LocalDate.of(YEAR, JANUARY, 21)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 23))
                        .endDate((LocalDate.of(YEAR, JANUARY, 28)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 29))
                        .endDate((LocalDate.of(YEAR, FEBRUARY, 3)))
                        .value(80L)
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 6)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 6))
                        .endDate((LocalDate.of(YEAR, JANUARY, 7)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 7))
                        .endDate((LocalDate.of(YEAR, JANUARY, 8)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 8))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 9))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 11))
                        .endDate((LocalDate.of(YEAR, JANUARY, 16)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 17))
                        .endDate((LocalDate.of(YEAR, JANUARY, 18)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 18))
                        .endDate((LocalDate.of(YEAR, JANUARY, 19)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 19))
                        .endDate((LocalDate.of(YEAR, JANUARY, 20)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 20))
                        .endDate((LocalDate.of(YEAR, JANUARY, 21)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 21))
                        .endDate((LocalDate.of(YEAR, JANUARY, 22)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 23))
                        .endDate((LocalDate.of(YEAR, JANUARY, 28)))
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 29))
                        .endDate((LocalDate.of(YEAR, FEBRUARY, 3)))
                        .value(80L)
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_7() {
        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 3)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 4))
                        .endDate((LocalDate.of(YEAR, JANUARY, 7)))
                        .value(80L)
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 2))
                        .endDate((LocalDate.of(YEAR, JANUARY, 5)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 6))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .value(80L)
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .value(80L)
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_8() {

        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 11))
                        .endDate((LocalDate.of(YEAR, JANUARY, 16)))
                        .value(100L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 17))
                        .endDate((LocalDate.of(YEAR, JANUARY, 22)))
                        .value(30L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 23))
                        .endDate((LocalDate.of(YEAR, JANUARY, 24)))
                        .value(10L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 25))
                        .endDate((LocalDate.of(YEAR, JANUARY, 26)))
                        .value(10L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 27))
                        .endDate((LocalDate.of(YEAR, JANUARY, 28)))
                        .value(10L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 30))
                        .endDate((LocalDate.of(YEAR, JANUARY, 31)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, FEBRUARY, 1))
                        .endDate((LocalDate.of(YEAR, FEBRUARY, 2)))
                        .value(80L)
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 6)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 7))
                        .endDate((LocalDate.of(YEAR, JANUARY, 8)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 9))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 11))
                        .endDate((LocalDate.of(YEAR, JANUARY, 16)))
                        .value(90L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 18))
                        .endDate((LocalDate.of(YEAR, JANUARY, 19)))
                        .value(20L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 20))
                        .endDate((LocalDate.of(YEAR, JANUARY, 21)))
                        .value(40L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 23))
                        .endDate((LocalDate.of(YEAR, JANUARY, 28)))
                        .value(100L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 29))
                        .endDate((LocalDate.of(YEAR, FEBRUARY, 3)))
                        .value(80L)
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 10)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 11))
                        .endDate((LocalDate.of(YEAR, JANUARY, 16)))
                        .value(90L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 17))
                        .endDate((LocalDate.of(YEAR, JANUARY, 18)))
                        .value(30L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 18))
                        .endDate((LocalDate.of(YEAR, JANUARY, 19)))
                        .value(20L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 19))
                        .endDate((LocalDate.of(YEAR, JANUARY, 20)))
                        .value(30L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 20))
                        .endDate((LocalDate.of(YEAR, JANUARY, 21)))
                        .value(40L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 21))
                        .endDate((LocalDate.of(YEAR, JANUARY, 22)))
                        .value(30L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 23))
                        .endDate((LocalDate.of(YEAR, JANUARY, 28)))
                        .value(100L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 29))
                        .endDate((LocalDate.of(YEAR, FEBRUARY, 3)))
                        .value(80L)
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_9() {

        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 2))
                        .endDate((LocalDate.of(YEAR, JANUARY, 5)))
                        .value(90L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 6))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .value(80L)
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 3)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 4))
                        .endDate((LocalDate.of(YEAR, JANUARY, 7)))
                        .value(80L)
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 3)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 3))
                        .endDate((LocalDate.of(YEAR, JANUARY, 4)))
                        .value(90L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 4))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .value(80L)
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    @Test
    public void testCombinePrices_10() {
        oldPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 3)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 4))
                        .endDate((LocalDate.of(YEAR, JANUARY, 7)))
                        .value(90L)
                        .build()
        );

        //--------------------------------------------------------------------------

        newPrices = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 2))
                        .endDate((LocalDate.of(YEAR, JANUARY, 5)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 6))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .value(90L)
                        .build()
        );

        final Collection<PriceModel> actual = priceCombiner.combine(oldPrices, newPrices);

        final Collection<PriceModel> expected = Arrays.asList(
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 1))
                        .endDate((LocalDate.of(YEAR, JANUARY, 5)))
                        .value(80L)
                        .build(),
                PriceModel.builder()
                        .beginDate(LocalDate.of(YEAR, JANUARY, 5))
                        .endDate((LocalDate.of(YEAR, JANUARY, 9)))
                        .value(90L)
                        .build()
        );

        Assert.assertThat(expected.size(), Is.is(actual.size()));
        Assert.assertTrue(expected.containsAll(actual));
    }

    private NavigableMap<DateRange, PriceModel> fill(final Collection<PriceModel> prices)
    {
        final NavigableMap<DateRange, PriceModel> pricesMap = new TreeMap<>(Comparator.comparing(DateRange::begin));
        return fill(pricesMap, prices);
    }

    private NavigableMap<DateRange, PriceModel> fill(final NavigableMap<DateRange, PriceModel> pricesMap,
                                                     final Collection<PriceModel> prices)
    {
        prices.forEach(price ->
                pricesMap.putIfAbsent(new DateRange(price.getBeginDate(), price.getEndDate()), price)
        );
        return pricesMap;
    }

}