package com.flyabrway.price.service;

import com.flyabrway.price.domain.model.PriceModel;

import java.util.Collection;

public interface PriceService {

    Collection<PriceModel> getProductPrices(Long productID);

}
