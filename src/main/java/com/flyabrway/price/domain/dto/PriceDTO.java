package com.flyabrway.price.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class PriceDTO {

    private Long ID;

    private String productCode;

    private Integer number;

    private Integer department;

    private LocalDate beginDate;

    private LocalDate endDate;

    private Long value;

}
