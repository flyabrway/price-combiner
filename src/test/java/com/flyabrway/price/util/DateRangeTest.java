package com.flyabrway.price.util;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class DateRangeTest {

    private static final Integer YEAR = 2000;
    private static final Integer PREV_YEAR = 1999;

    @Test(expected = IllegalArgumentException.class)
    public void Should_Throw_Exception_When_Passed_Null_Date() {
        // given
        final LocalDate begin = LocalDate.of(YEAR, Month.JANUARY, 1);
        final LocalDate end = null;

        // when
        new DateRange(begin, end);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Should_Throw_Exception_When_Negative_Range() {
        // given
        final LocalDate begin = LocalDate.of(YEAR, Month.JANUARY, 2);
        final LocalDate end = LocalDate.of(YEAR, Month.JANUARY, 1);

        // when
        new DateRange(begin, end);
    }

    @Test
    public void Is_Empty_When_Begin_Equals_End() {
        boolean isEmpty = true;

        // given
        final LocalDate begin = LocalDate.of(YEAR, Month.JANUARY, 1);
        final LocalDate end = LocalDate.of(YEAR, Month.JANUARY, 1);

        // when
        boolean result = new DateRange(begin, end).isEmpty();

        // then
        Assert.assertThat(result, Is.is(equalTo(isEmpty)));
    }

    @Test
    public void Is_Not_Empty_When_Begin_Not_Equals_End() {
        boolean isEmpty = false;

        // given
        final LocalDate begin = LocalDate.of(YEAR, Month.JANUARY, 1);
        final LocalDate end = LocalDate.of(YEAR, Month.JANUARY, 2);

        // when
        boolean result = new DateRange(begin, end).isEmpty();

        // then
        Assert.assertThat(result, Is.is(equalTo(isEmpty)));
    }

    @Test
    public void Should_Return_True_Included_Date() {
        boolean isIncluded = true;

        // given
        final LocalDate included = LocalDate.of(YEAR, Month.JANUARY, 5);
        final DateRange dateRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // when
        boolean result = dateRange.includeDate(included);

        // then
        Assert.assertThat(result, Is.is(equalTo(isIncluded)));
    }

    @Test
    public void Should_Return_True_Border_Included_Date() {
        boolean isIncluded = true; 

        // given
        final LocalDate included = LocalDate.of(YEAR, Month.JANUARY, 1);
        final DateRange dateRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // when
        boolean result = dateRange.includeDate(included);

        // then
        Assert.assertThat(result, Is.is(equalTo(isIncluded)));
    }

    @Test
    public void Should_Return_False_Excluded_Date() {
        boolean isIncluded = false;

        // given
        final LocalDate excluded = LocalDate.of(YEAR, Month.JANUARY, 15);
        final DateRange dateRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // when
        boolean result = dateRange.includeDate(excluded);

        // then
        Assert.assertThat(result, Is.is(equalTo(isIncluded)));
    }

    @Test
    public void Should_Return_True_Included_Range() {
        boolean isIncluded = true;

        // given
        final DateRange outerRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange innerRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 5)
        );

        // when
        boolean result = outerRange.includeRange(innerRange);

        // then
        Assert.assertThat(result, Is.is(equalTo(isIncluded)));
    }

    @Test
    public void Should_Return_True_Border_Included_Range() {
        boolean isIncluded = true;

        // given
        final DateRange outerRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange innerRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // when
        boolean result = outerRange.includeRange(innerRange);

        // then
        Assert.assertThat(result, Is.is(equalTo(isIncluded)));
    }

    @Test
    public void Should_Return_False_Excluded_Range() {
        boolean isIncluded = false;

        // given
        final DateRange outerRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange innerRange = new DateRange(
                LocalDate.of(PREV_YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 5)
        );

        // when
        boolean result = outerRange.includeRange(innerRange);

        // then
        Assert.assertThat(result, Is.is(equalTo(isIncluded)));
    }

    @Test
    public void Should_Return_Outer_Range() {
        final Optional<DateRange> outerRangeUpper = Optional.of(
                new DateRange(LocalDate.of(YEAR, Month.JANUARY, 8), LocalDate.of(YEAR, Month.JANUARY, 10))
        );

        final Optional<DateRange> outerRangeLower = Optional.of(
                new DateRange(LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 5))
        );

        // given
        final DateRange externalRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange internalRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 8)
        );

        // when
        final Optional<DateRange> resultHigher = externalRange.searchOuterRange(internalRange, true);
        final Optional<DateRange> resultLower = externalRange.searchOuterRange(internalRange, false);

        // then
        Assert.assertThat(resultHigher, Is.is(equalTo(outerRangeUpper)));
        Assert.assertThat(resultLower, Is.is(equalTo(outerRangeLower)));
    }

    @Test
    public void Should_Return_Outer_Range_Which_Is_Empty() {
        final Optional<DateRange> outerRange = Optional.of(
                new DateRange(
                        LocalDate.of(YEAR, Month.JANUARY, 10), LocalDate.of(YEAR, Month.JANUARY, 10)
                )
        );
        final boolean shouldBeEmptyOuterRange = true;

        // given
        final DateRange externalRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange internalRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // when
        final Optional<DateRange> resultHigher = externalRange.searchOuterRange(internalRange, true);

        // then
        Assert.assertThat(resultHigher, Is.is(equalTo(outerRange)));
        Assert.assertThat(resultHigher.orElseThrow(NoSuchElementException::new).isEmpty(), Is.is(equalTo(shouldBeEmptyOuterRange)));
    }

    @Test
    public void Should_Return_True_Overlapped_Range() {
        boolean isOverlapped = true;

        // given
        final DateRange externalRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange internalRange = new DateRange(
                LocalDate.of(PREV_YEAR, Month.DECEMBER, 31), LocalDate.of(YEAR, Month.JANUARY, 5)
        );

        // when
        boolean result = externalRange.overlap(internalRange);

        // then
        Assert.assertThat(result, Is.is(equalTo(isOverlapped)));
    }

    @Test
    public void Should_Return_False_Non_Overlapped_Range() {
        boolean isOverlapped = false;

        // given
        final DateRange externalRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange internalRange = new DateRange(
                LocalDate.of(PREV_YEAR, Month.DECEMBER, 1), LocalDate.of(PREV_YEAR, Month.DECEMBER, 31)
        );

        // when
        boolean result = externalRange.overlap(internalRange);

        // then
        Assert.assertThat(result, Is.is(equalTo(isOverlapped)));
    }

    @Test
    public void Should_Return_Higher_Non_Overlapped_Range() {
        final Optional<DateRange> foundNonOverlappedRange = Optional.of(
                new DateRange(
                        LocalDate.of(YEAR, Month.JANUARY, 10), LocalDate.of(YEAR, Month.JANUARY, 15)
                )
        );

        // given
        final DateRange lower = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange higher = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 15)
        );

        // when
        final Optional<DateRange> result = lower.searchNonOverlap(higher, true);

        // then
        Assert.assertThat(result, Is.is(equalTo(foundNonOverlappedRange)));
    }

    @Test
    public void Should_Return_Lower_Non_Overlapped_Range() {
        final Optional<DateRange> foundNonOverlappedRange = Optional.of(
                new DateRange(
                        LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 5)
                )
        );

        // given
        final DateRange lower = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange higher = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 15)
        );

        // when
        final Optional<DateRange> result = lower.searchNonOverlap(higher, false);

        // then
        Assert.assertThat(result, Is.is(equalTo(foundNonOverlappedRange)));
    }

    @Test
    public void Shoud_Find_Non_Overlapped_Range_Higher_Which_Is_Empty() {
        final Optional<DateRange> emptyNonOverlappedRange = Optional.of(
                new DateRange(
                        LocalDate.of(YEAR, Month.JANUARY, 10), LocalDate.of(YEAR, Month.JANUARY, 10)
                )
        );
        final boolean shouldBeEmptyNonOverlappedRange = true;

        // given
        final DateRange lower = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange higher = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // when
        final Optional<DateRange> result = lower.searchNonOverlap(higher, true);

        // then
        Assert.assertThat(result, Is.is(equalTo(emptyNonOverlappedRange)));
        Assert.assertThat(result.orElseThrow(NoSuchElementException::new).isEmpty(), Is.is(equalTo(shouldBeEmptyNonOverlappedRange)));
    }

    @Test
    public void Should_Return_Gap_Between_Ranges() {
        final Optional<DateRange> shouldReturnGap = Optional.of(
                new DateRange(LocalDate.of(YEAR, Month.JANUARY, 10), LocalDate.of(YEAR, Month.JANUARY, 15))
        );

        // given
        final DateRange lowerRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange higherRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 15), LocalDate.of(YEAR, Month.JANUARY, 31)
        );

        // when
        final Optional<DateRange> resultGapRangeOne = higherRange.gap(lowerRange);
        final Optional<DateRange> resultGapRangeTwo = lowerRange.gap(higherRange);

        // then
        Assert.assertThat(resultGapRangeOne, is(equalTo(shouldReturnGap)));
        Assert.assertThat(resultGapRangeTwo, is(equalTo(shouldReturnGap)));
        Assert.assertThat(resultGapRangeOne, is(equalTo(resultGapRangeTwo)));
    }

    @Test
    public void Should_Return_Empty_Gap_Between_Overlapped_Ranges() {
        final Optional<DateRange> emptyGap = Optional.empty();

        // given
        final DateRange lowerRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange higherRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 15)
        );

        // when
        final Optional<DateRange> resultGapRangeOne = higherRange.gap(lowerRange);
        final Optional<DateRange> resultGapRangeTwo = lowerRange.gap(higherRange);

        // then
        Assert.assertThat(resultGapRangeOne, Is.is(equalTo(emptyGap)));
        Assert.assertThat(resultGapRangeOne, Is.is(equalTo(resultGapRangeTwo)));
    }

    @Test
    public void Should_Return_Splitted_Ranges() {
        final List<DateRange> splittedRanges = Stream.of(
                new DateRange(LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 4)),
                new DateRange(LocalDate.of(YEAR, Month.JANUARY, 4), LocalDate.of(YEAR, Month.JANUARY, 6)),
                new DateRange(LocalDate.of(YEAR, Month.JANUARY, 6), LocalDate.of(YEAR, Month.JANUARY, 10))
        ).collect(Collectors.toList());

        // given
        final DateRange toSplit = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange splitter = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 4), LocalDate.of(YEAR, Month.JANUARY, 6)
        );

        // when
        final List<DateRange> resultSplittedRanges = toSplit.split(splitter);

        // then
        Assert.assertThat(resultSplittedRanges, Is.is(equalTo(splittedRanges)));
    }

    @Test
    public void Should_Not_Return_Splitted_Ranges() {
        final List<DateRange> shouldReturnSplittedRanges = Collections.emptyList();

        // given
        final DateRange toSplit = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange splitter = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 5), LocalDate.of(YEAR, Month.JANUARY, 15)
        );

        // when
        final List<DateRange> resultSplittedRanges = toSplit.split(splitter);

        // then
        Assert.assertThat(resultSplittedRanges, Is.is(equalTo(shouldReturnSplittedRanges)));
    }

    @Test
    public void Should_Return_Expanded_Range_Between_Two_Ranges() {
        final DateRange expandedRange = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // given
        final DateRange first = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );
        final DateRange second = new DateRange(
                LocalDate.of(YEAR, Month.JANUARY, 1), LocalDate.of(YEAR, Month.JANUARY, 10)
        );

        // when
        final DateRange result = second.expand(first);

        // then
        Assert.assertThat(expandedRange, Is.is(equalTo(result)));
    }

}